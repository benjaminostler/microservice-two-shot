function HatList({hats}) {
  return (
      <table className="table table-striped">
      <thead>
        <tr>
          <th>Style Name</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hats?.map(hat => {
          return (
            <tr key={hat.id}>
              <td>{ hat.style_name }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.color }</td>
              <td><img alt="" src={hat.picture_url}/></td>
              <td>{ hat.location_vo.closet_name }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatList;
