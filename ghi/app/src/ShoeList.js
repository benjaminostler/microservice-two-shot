function ShoeList({shoe, getShoe}) {

    const deleteShoe = (id) => {
        fetch(`http://localhost:8080/api/shoes/${id}/`, {
            method: "delete",
        })
        .then(() => {
            return getShoe()
        })
        .catch(console.log)
    }

    if (shoe === undefined) {
        return null;
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {shoe.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td><img src={ shoe.picture_url } width= '100' height= '100'></img></td>
                            <td>{ shoe.bin_number }</td>
                            <td><button onClick={() => deleteShoe(shoe.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList;
