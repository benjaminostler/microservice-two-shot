import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App() {
  const [hats, setHats] = useState([]);
  const [locations, setLocations ] = useState("");

  async function getLocations() {
      const response = await fetch('http://localhost:8100/api/locations/');
      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
  }
  const [shoe, setShoe] = useState([]);

  async function loadHats() {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
    else{
      console.error("Response was not okay");
    }
  }

  const getShoe = async () => {
    console.log("getShoe is working in App.js")
    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const shoeResponse = await fetch(shoeUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoe = data.shoe
      setShoe(shoe)
    }
    else{
      console.error("Response was not okay");
    }
  }

  useEffect(() => {
    loadHats();
    getLocations();

    getShoe();
  }, []);

  if (hats === undefined){
    return null;
  }
  return (

    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={hats}/>} />
            <Route path="new" element={<HatForm locations={locations}/>} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList shoe={shoe} getShoe={getShoe} />} />
            <Route path="new" element={<ShoeForm getShoe={getShoe}/>} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
