import { useEffect, useState } from 'react';

function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value)
    }
    const handleModelNameChange = (e) => {
        const value = e.target.value;
        setModelName(value)
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value)
    }
    const handlePictureChange = (e) => {
        const value = e.target.value;
        setPicture(value)
    }
    const handleBinChange = (e) => {
        const value = e.target.value;
        setBin(value)
    }

    useEffect(() => {
        async function getBins() {
            const url = 'http://localhost:8100/api/bins';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            }
        }
        getBins();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = picture;
        data.bin = bin;
        console.log(data);

        const shoeUrl = `http://localhost:8080/api/shoes/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json();
            setManufacturer('')
            setModelName('')
            setColor('')
            setPicture('')
            setBin('')
        }
    }

    
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleModelNameChange} value={modelName} placeholder="Model name" required type="text" name="model" id="manufacturer" className="form-control"/>
                    <label htmlFor="model_name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} value={picture} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control"/>
                    <label htmlFor="picture">Picture</label>
                </div>
                <div className="mb-3">
                    <select required onChange={handleBinChange} name="bin" id="bin" className="form-select" value={bin} >
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.id} value={bin.href}>
                                    {bin.bin_number}
                                </option>
                             );
                            })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>

    )

}

export default ShoeForm;
