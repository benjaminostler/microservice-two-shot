import React, { useState } from 'react';


function HatForm({locations}) {
    const [styleName, setStyleName ] = useState("");
    const [fabric, setFabric ] = useState("");
    const [color, setColor ] = useState("");
    const [pictureUrl, setPictureUrl ] = useState("");
    const [location, setLocation ] = useState("");

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            style_name: styleName,
            fabric,
            color,
            picture_url: pictureUrl,
            location,
        };

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch('http://hats-api:8000/api/hats/', fetchConfig);
        if (response.ok) {

            setStyleName('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    }

    function handleChangeStyleName(event) {
    const value = event.target.value;
    setStyleName(value);
    }

    function handleChangeFabric(event) {
    const value = event.target.value;
    setFabric(value);
    }

    function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
    }

    function handleChangePictureUrl(event) {
    const value = event.target.value;
    setPictureUrl(value);
    }

    function handleChangeLocation(event) {
    const value = event.target.value;
    setLocation(value);
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChangeStyleName} value={styleName} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                    <label htmlFor="style_name">Style Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChangeFabric} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChangePictureUrl} value={pictureUrl} placeholder="Picture Url" required type="href" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture Url</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleChangeLocation} value={location} required name="location" id="location" className="form-select">
                        <option value="">Choose a location</option>
                        {locations?.map(location => {
                            return (
                                <option key={location.id} value={location.href}>{location.closet_name}</option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default HatForm;
