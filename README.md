# Wardrobify

Team:

* Benjamin Ostler - hats microservice
* Cindy Lam- shoes microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

CINDY, I copied and pasted the following from the project instructions in case you want to use it as reference for writing your explanation of the Shoes microservice.

You can access the Django shoe API application from your browser or Insomnia on port 8080.
There are two Shoes microservices:
shoes/api is a Django application with a Django project and a Django app already created.
- The Django app is not installed in the Django project.
- There are no URLs, views, or models yet created. You must create these.

shoes/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file, shoes/poll/poller.py, that you must implement to pull Bin data from the Wardrobe API.

The Shoe resource should track its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists.
- You must create RESTful APIs to get a list of shoes, create a new shoe, and delete a shoe.
- You must create React component(s) to show a list of all shoes and their details.
- You must create React component(s) to show a form to create a new shoe.
- You must provide a way to delete a shoe.
- You must route the existing navigation links to your components.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
Models:
Hat
    - fabric        // fabric hat is made of
    - style_name    // style name of hat
    - color         // color of hat
    - picture_url   // url of hat picture
    - location_vo   // Wardrobe where Hat exists

LocationVO:             // wardrobe where hats exist
    - closet_name       // closet in wardrobe
    - section_number    // section in closet
    - shelf_number      // shelf number in section
    - import_href       // href of location imported from wardrobe models

You can access the Django hat API application from your browser or Insomnia on port 8090

There are two Hats microservices:

hats/api is a Django application with a Django project and a Django app.
hats/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file, hats/poll/poller.py, which pulls Location data from the Wardrobe API.

RESTful APIs that get a list of hats, create a new hat, and delete a hat.
React components to show a list of all hats with their details and a form to create a new hat.
Provides a way to delete a hat.
Navigation links are routed to the React components.
