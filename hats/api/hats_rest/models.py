from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=300, unique=True)

    def __str__(self):
        return f"{self.closet_name} - Section {self.section_number} Shelf {self.shelf_number}"


class Hat(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)
    location_vo = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_hat", kwargs={"location": self.location_vo_id})
