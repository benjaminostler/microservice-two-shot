from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from .models import LocationVO, Hat
import json
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class LocationVOListEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "id",
        "color",
        "picture_url",
        "location_vo",
    ]

    encoders = {
        "location_vo": LocationVODetailEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location_vo",
    ]
    encoders = {
        "location_vo": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
# def api_hats(request, location_vo_id=None):
def api_hats(request, location_vo_id=None):
    if request.method == "GET":
        # if location_vo_id is None:
        hats = Hat.objects.all()
        # else:
        #     hats = Hat.objects.filter(location=location_vo_id)
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:  # POST
        content = json.loads(request.body)
        print("New Hat Content:", content)  #
        # Content: {'style_name': 'dfs', 'fabric': 'wsgw', 'color': 'wsgbw', 'picture_url':
        # 'https://assets.yuengling.com/wp-content/uploads/2022/02/29123354/2296-Yueng-Sewn-Patch-Hat-19-scaled.jpg',
        # 'location': '/api/locations/6/'}
        try:
            location = LocationVO.objects.get(id=location_vo_id)
            # print("Location:", location)  # comes back as "Location: bananas - Section 2 Shelf 1"
            content["location_vo"] = location
            # print(content["location_vo"])  # comes back as "bananas - Section 2 Shelf 1"
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        print("updated content type:", type(content["location_vo"]))  # comes back as "updated content type: <class 'hats_rest.models.LocationVO'>"
        print("updated content:", content["location_vo"])  # comes back as "updated content: bananas - Section 2 Shelf 1"
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.filter(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
        )
    elif request.method == "DELETE":
        print("Trying to delete hat")
        try:
            count, _ = Hat.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            Hat.objects.filter(id=id).update(**content)
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
