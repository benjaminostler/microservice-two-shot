from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "bin_number"
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
            "manufacturer",
            "model_name",
            "color",
            "picture_url",
            "id",
    ]

    def get_extra_data(self, o):
        return {
            "bin_number": o.bin.bin_number,
        }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'bin',
    ]
    encoders = {
        'bin': BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoe = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
            print(content)
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id!!!"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoes(request, pk):
    shoe = Shoe.objects.get(pk=pk)
    if request.method == "GET":
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(pk=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        if "bin" in content:
            try:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
            except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid bin id!!!"},
                    status=400,
                )
