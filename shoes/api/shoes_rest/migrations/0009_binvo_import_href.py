# Generated by Django 4.0.3 on 2023-06-02 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0008_remove_binvo_import_href'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='import_href',
            field=models.CharField(max_length=300, null=True, unique=True),
        ),
    ]
