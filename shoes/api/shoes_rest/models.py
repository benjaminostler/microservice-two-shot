from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=300, unique=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoe",
        on_delete=models.PROTECT,
        null=True)

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
